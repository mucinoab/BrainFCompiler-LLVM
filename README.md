# BrainFCompiler-LLVM
A [Brainfuck](https://en.wikipedia.org/wiki/Brainfuck) toy compiler that generates an ELF file through [LLVM](https://en.wikipedia.org/wiki/LLVM) and links it using gcc.

## Requirements

- OS ~Linux
- Linker [gcc](https://gcc.gnu.org/)
- [Rust](https://www.rust-lang.org/)


## Usage

You just clone the repo and run, ```cargo run --release _sourcefile.bf_```

